import { Component } from '@angular/core';
import { NavController } from '@ionic/angular';
import { HttpHeaders } from '@angular/common/http';
import { RequestService } from 'src/app/services/RequestService';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  public wJson = {
    city: 'Bogotá',
    wheather: 'Soleado',
    posibility: 'Posibilidad de lluvia',
    grade: 20
  };
  public oil = {
    price: 60.81,
    formatted: '$60.81',
    currency: 'USD',
    code: 'OIL_BRENT_USD',
  };
  constructor(
    private navCtrl: NavController,
    private rs: RequestService,
    public storage: Storage) {
    this.loadWheather();
  }

  goToPQR = () => this.navCtrl.navigateForward('questions');

  loadWheather() {
    // tslint:disable-next-line: max-line-length
    const url = 'https://cors-anywhere.herokuapp.com/https://weather.api.here.com/weather/1.0/report.json?app_id=U3EG0A9hrCXQ6Bgo3TYo&app_code=16PKPXLOkpvv-2vLiv3W8g&latitude=4.618789&longitude=-74.067911&product=observation';
    const body = ``;

    const headers = new HttpHeaders({
      'Content-Type': 'application/json'
    });

    this.rs.request('GET', url, body, headers).then(r => {
      this.wJson.city = r.observations.location[0].state;
      this.wJson.posibility = r.observations.location[0].observation[0].description;
      this.wJson.wheather = r.observations.location[0].observation[0].temperatureDesc;
      // tslint:disable-next-line: radix
      this.wJson.grade = parseInt(r.observations.location[0].observation[0].temperature, 0);
    });
  }

  loadOil() {
    const url = 'https://api.oilpriceapi.com/v1/prices/latest';
    const body = ``;

    const headers = new HttpHeaders({
      Authorization: 'Token 74f558b1a83a75af4122adef27b78380'
    });

    this.rs.request('GET', url, body, headers).then(r => {
      this.oil.price = r.data.price;
      this.oil.formatted = r.data.formatted;
      this.oil.currency = r.data.currency;
      this.oil.code = r.data.code;
    });
  }

}
