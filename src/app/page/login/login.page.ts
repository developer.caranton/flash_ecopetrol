import { Component, OnInit } from '@angular/core';
import { NavController, AlertController } from '@ionic/angular';
import { RequestService } from 'src/app/services/RequestService';
import { async } from 'q';
import { HttpHeaders } from '@angular/common/http';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage {
  public login = {
    username: '',
    password: ''
  };

  public password = '';
  constructor(
    private navCtrl: NavController,
    private rs: RequestService,
    private storage: Storage,
    public a: AlertController) { }


  changeUser = (value) => {
    this.login.username = value;
  }

  changePass = (value) => {
    this.login.password = value;
  }

  async presentToast(s: string) {
    const alert = await this.a.create({
      header: 'Ups!!',
      subHeader: '',
      message: s,
      buttons: ['Volver a intentar']
    });

    alert.present();
  }

  sendLogin() {
    if (this.login.username === '' || this.login.password === '') {
      return;
    }
    const url = 'https://apim-dev-fabpersonalizacion.azure-api.net/seguridad/api/v1/Seguridad/usuario';
    const body = `{"nickname":"${this.login.username}","passw":"${this.login.password}"}`;

    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Ocp-Apim-Subscription-Key': '9f76128256044de186780a5d8be1eefd',
      'Ocp-Apim-Trace': 'True',
      Authorization: 'token'
    });

    this.rs.request('POST', url, body, headers).then(async r => {

      if (r.datos.token) {
        // set a key/value
        this.storage.set('token', r.datos.token);
        this.navCtrl.navigateForward('home');
      }
    }).catch(async f => {
      this.presentToast(f.error.mensaje);
    });
  }
}
