import { Component, OnInit } from '@angular/core';
import { RequestService } from 'src/app/services/RequestService';
import { HttpHeaders } from '@angular/common/http';
import { Storage } from '@ionic/storage';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-questions',
  templateUrl: './questions.page.html',
  styleUrls: ['./questions.page.scss'],
})
export class QuestionsPage implements OnInit {
  public list = [];
  public isLoaded = false;
  constructor(
    private rs: RequestService,
    private storage: Storage,
    private navCtrl: NavController) {
    this.getInfo();
  }

  ngOnInit() {

  }

  getInfo() {
    const url = 'http://52.225.195.29:8082/api/v1/FAQs/FAQ';
    const body = ``;

    const headers = new HttpHeaders({
      'Content-Type': 'application/json'
    });

    this.rs.request('GET', url, body, headers).then(r => {
      if (r && r.datos) {
        r.datos.forEach(e => {
          console.log(e.pregunta);
          const obj = {
            id: e.id,
            pregunta: e.pregunta ? e.pregunta : ''
          };
          this.list.push(obj);
        });
        this.isLoaded = true;
      }
    }).catch(f => {
      console.log('f', f);
      this.storage.set('pqrs', []);
      // this.presentToast(f.error.mensaje);
    });
  }

  bkBtn() {
    this.navCtrl.navigateBack('home');
  }

  openItem = (id: number) => {
    this.closedAllItems();
    const showItem = document.getElementById(`item-${id}`);
    showItem.classList.add('active');
    showItem.classList.remove('inactive');

    const url = `http://52.225.195.29:8082/api/v1/Respuesta/respuestas/${id}`;
    const body = ``;

    const headers = new HttpHeaders({
      'Content-Type': 'application/json'
    });

    this.rs.request('GET', url, body, headers).then(r => {
      if (r && r.datos) {
        const showItemDesc = document.getElementById(`itemDes-${id}`);
        showItemDesc.classList.add('activeDesc');
        showItemDesc.classList.remove('inactiveDesc');
        showItemDesc.innerHTML = r.datos.respuesta;
      }
    }).catch(f => {
      console.log('f', f);
      const showItemDesc = document.getElementById(`itemDes-${id}`);
      showItemDesc.classList.add('activeDesc');
      showItemDesc.classList.remove('inactiveDesc');
      showItemDesc.innerHTML = 'Sín respuesta';
      // this.presentToast(f.error.mensaje);
    });
  }

  closedAllItems = () => {
    const itemAccordeonTitle = document.querySelectorAll('.itemAccordeonTitle');
    [].forEach.call(itemAccordeonTitle, (el: any) => {
      el.classList.add('inactive');
      el.classList.remove('active');
    });

    const desAccordeon = document.querySelectorAll('.desAccordeon');
    [].forEach.call(desAccordeon, (el: any) => {
      el.classList.add('inactiveDesc');
      el.classList.remove('activeDesc');
    });
  }
}
