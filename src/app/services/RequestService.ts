import { HttpClient, HttpRequest, HttpHeaders } from '@angular/common/http';


export class RequestService {
    constructor(private http: HttpClient) {

    }

    request(m: string, url: string, body: string, h): Promise<any> {
        const prom = new Promise((resolve, reflect) => {
            const request = new HttpRequest(m, url, body, { headers: h });
            this.http.request(request).toPromise()
                .then((responseData: any) => {
                    resolve(responseData.body);
                }).catch((err) => {
                    reflect(err);
                });
        });
        return prom;
    }
}
